const Vehicle = require("./index.js");

const vehicle1 = new Vehicle({
  brand: "Toyota",
  model: "Innova",
  type: "Medium MPV",
  engine: "Diesel",
  capacity: "2.4 litre turbodiesel",
});
vehicle1.speed = 100;
vehicle1.cruiseControl(20);
