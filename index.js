class Vehicle {
  constructor({ brand, model, type, engine, capacity }) {
    this.brand = brand;
    this.model = model;
    this.type = type;
    this.engine = engine;
    this.capacity = capacity;
  }
  speed;
  cruiseControl(faster = 0, slower = 0) {
    faster === 0 ? (this.speed -= slower) : (this.speed += faster);
  }
}

module.exports = Vehicle;
